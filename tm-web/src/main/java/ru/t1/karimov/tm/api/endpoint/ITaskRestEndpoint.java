package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.Task;

import java.util.Collection;

public interface ITaskRestEndpoint {

    @NotNull
    Collection<Task> findAll();

    @Nullable
    Task findById(String id);

    @NotNull
    Task save(Task task);

    void delete(Task task);

    void delete(String id);

}
