package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.Project;

import java.util.Collection;

public interface IProjectRestEndpoint {

    @NotNull
    Collection<Project> findAll();

    @Nullable
    Project findById(String id);

    @NotNull
    Project save(Project project);

    void delete(Project project);

    void delete(String id);

}
