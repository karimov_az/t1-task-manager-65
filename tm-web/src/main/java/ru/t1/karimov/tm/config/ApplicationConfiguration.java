package ru.t1.karimov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.karimov.tm")
public class ApplicationConfiguration {
}
