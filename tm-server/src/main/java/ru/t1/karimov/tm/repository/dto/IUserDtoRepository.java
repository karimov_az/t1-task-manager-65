package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.dto.model.UserDto;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

}
